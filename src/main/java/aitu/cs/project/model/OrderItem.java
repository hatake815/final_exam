package aitu.cs.project.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.List;

@Data
@NoArgsConstructor
@Entity
@Table(name = "orderitem")
public class OrderItem {
    @Id
    private int id;
    private int orderId;
    private int productId;
    private int quantity;
    private float price;

    @Transient
    private int quantityNotInStorage;

    public int getQuantityNotInStorage() {
        return quantityNotInStorage;
    }

    public void setQuantityNotInStorage(int quantityNotInStorage) {
        this.quantityNotInStorage = quantityNotInStorage;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }
}
