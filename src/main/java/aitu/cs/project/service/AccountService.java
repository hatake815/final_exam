package aitu.cs.project.service;

import aitu.cs.project.model.Account;
import aitu.cs.project.model.Customer;
import aitu.cs.project.repository.AccountRepository;
import aitu.cs.project.repository.CustomerRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;

@Service
public class AccountService {
    private final AccountRepository accountRepository;
    private final CustomerRepository customerRepository;

    public AccountService(AccountRepository accountRepository, CustomerRepository customerRepository) {
        this.accountRepository = accountRepository;
        this.customerRepository = customerRepository;
    }

    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(accountRepository.findAll());
    }

    public ResponseEntity<?> getByID(int id){
        return ResponseEntity.ok(accountRepository.findById(id));
    }

    public void deleteByID(int id){
        accountRepository.deleteById(id);
    }

    public Account save(Account account){
        return accountRepository.save(account);
    }

    @Transactional
    public Account signIn(Account account) throws Exception {
        String login = account.getLogin();
        String password = account.getPassword();
        Account account1 =  accountRepository.findByLoginAndPassword(login, password);
        if (account1 == null){
            throw new Exception("Not authorized!");
        }
        String token = UUID.randomUUID().toString();
        account1.setToken(token);
        accountRepository.save(account1);
        return account1;
    }

    public Account register(Account account) throws Exception{
        String login = account.getLogin();
        String password = account.getPassword();
        Account acc = accountRepository.findByLoginAndPassword(login, password);
        if (acc == null) {
            String role = "customer";
            String token = UUID.randomUUID().toString();
            accountRepository.insertAccount(role, login, password, token);
            return accountRepository.findByToken(token);
        } else {
            throw new Exception("Not authorized!");
        }
    }
}
