package aitu.cs.project.service;

import aitu.cs.project.model.Model;
import aitu.cs.project.model.Product;
import aitu.cs.project.repository.ModelRepository;
import aitu.cs.project.repository.ProductRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ModelService {
    private final ModelRepository modelRepository;
    private final ProductRepository productRepository;

    public ModelService(ModelRepository modelRepository, ProductRepository productRepository) {
        this.modelRepository = modelRepository;
        this.productRepository = productRepository;
    }

    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(modelRepository.findAll());
    }

    public ResponseEntity<?> getByID(int id){
        return ResponseEntity.ok(modelRepository.findById(id));
    }

    public void deleteByID(int id){
        modelRepository.deleteById(id);
    }

    public Model save(Model model){
        return modelRepository.save(model);
    }

}
