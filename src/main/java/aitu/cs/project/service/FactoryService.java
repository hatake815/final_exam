package aitu.cs.project.service;

import aitu.cs.project.model.Employee;
import aitu.cs.project.model.Factory;
import aitu.cs.project.repository.FactoryRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class FactoryService {
    private final FactoryRepository factoryRepository;

    public FactoryService(FactoryRepository factoryRepository) {
        this.factoryRepository = factoryRepository;
    }

    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(factoryRepository.findAll());
    }

    public ResponseEntity<?> getByID(int id){
        return ResponseEntity.ok(factoryRepository.findById(id));
    }

    public void deleteByID(int id){
        factoryRepository.deleteById(id);
    }

    public Factory save(Factory factory){
        return factoryRepository.save(factory);
    }
}
