package aitu.cs.project.service;

import aitu.cs.project.model.Delivery;
import aitu.cs.project.repository.DeliveryRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class DeliveryService {
    private final DeliveryRepository deliveryRepository;

    public DeliveryService(DeliveryRepository deliveryRepository) {
        this.deliveryRepository = deliveryRepository;
    }

    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(deliveryRepository.findAll());
    }

    public ResponseEntity<?> getByID(int id){
        return ResponseEntity.ok(deliveryRepository.findById(id));
    }

    public void deleteByID(int id){
        deliveryRepository.deleteById(id);
    }

    public Delivery save(Delivery delivery){
        return deliveryRepository.save(delivery);
    }
}
