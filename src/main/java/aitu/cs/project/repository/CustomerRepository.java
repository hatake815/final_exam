package aitu.cs.project.repository;

import aitu.cs.project.model.Customer;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface CustomerRepository extends CrudRepository<Customer, Integer> {
    Customer findByAccountId(int accountId);

    Customer findByCustomerId(int customerId);

    @Transactional
    @Modifying
    @Query(
            value = "insert into customer (account_id, organization_name, address, contact_number, email) values (:account_id, :organization_name, :address, :contact_number, :email)",
            nativeQuery = true)
    void insertCustomer(@Param("account_id") int accountId, @Param("organization_name") String organizationName ,
                       @Param("address") String address, @Param("contact_number") String contactNumber, @Param("email") String email);
}
