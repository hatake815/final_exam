package aitu.cs.project.repository;

import aitu.cs.project.model.Factory;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FactoryRepository extends CrudRepository<Factory, Integer> {
    Factory findByFactoryId(int id);
}
