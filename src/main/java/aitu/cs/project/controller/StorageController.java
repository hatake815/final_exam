package aitu.cs.project.controller;

import aitu.cs.project.model.Product;
import aitu.cs.project.model.Storage;
import aitu.cs.project.service.StorageService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class StorageController {
    private final StorageService storageService;

    public StorageController(StorageService storageService) {
        this.storageService = storageService;
    }

    @GetMapping("/storage")
    public ResponseEntity<?> getAll(){
        return storageService.getAll();
    }

    @GetMapping("/storage/{id}")
    public ResponseEntity<?> getByID(@PathVariable int id){
        return storageService.getByID(id);
    }

    @DeleteMapping("/storage/{id}")
    public void deleteByID(@PathVariable int id){
        storageService.deleteByID(id);
    }

    @PostMapping("/storage")
    public ResponseEntity<?> create(@RequestBody Storage storage){
        return ResponseEntity.ok(storageService.save(storage));
    }
}
