package aitu.cs.project.controller;

import aitu.cs.project.model.Employee;
import aitu.cs.project.model.Factory;
import aitu.cs.project.service.FactoryService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class FactoryController {
    private final FactoryService factoryService;

    public FactoryController(FactoryService factoryService) {
        this.factoryService = factoryService;
    }

    @GetMapping("/factory")
    public ResponseEntity<?> getAll(){
        return factoryService.getAll();
    }

    @GetMapping("/factory/{id}")
    public ResponseEntity<?> getByID(@PathVariable int id){
        return factoryService.getByID(id);
    }

    @DeleteMapping("/factory/{id}")
    public void deleteByID(@PathVariable int id){
        factoryService.deleteByID(id);
    }

    @PostMapping("/factory")
    public ResponseEntity<?> create(@RequestBody Factory factory){
        return ResponseEntity.ok(factoryService.save(factory));
    }
}
