package aitu.cs.project.controller;

import aitu.cs.project.model.Delivery;
import aitu.cs.project.model.Employee;
import aitu.cs.project.service.EmployeeService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class EmployeeController {
    private final EmployeeService employeeService;

    public EmployeeController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @GetMapping("/employee")
    public ResponseEntity<?> getAll(){
        return employeeService.getAll();
    }

    @GetMapping("/employee/{id}")
    public ResponseEntity<?> getByID(@PathVariable int id){
        return employeeService.getByID(id);
    }

    @DeleteMapping("/employee/{id}")
    public void deleteByID(@PathVariable int id){
        employeeService.deleteByID(id);
    }

    @PostMapping("/employee")
    public ResponseEntity<?> create(@RequestBody Employee employee){
        return ResponseEntity.ok(employeeService.save(employee));
    }
}
