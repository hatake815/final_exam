package aitu.cs.project.controller;

import aitu.cs.project.model.CustomerOrder;
import aitu.cs.project.model.Model;
import aitu.cs.project.model.OrderItem;
import aitu.cs.project.service.OrderItemService;
import com.fasterxml.jackson.databind.util.JSONPObject;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

@RestController
public class OrderItemController {
    private final OrderItemService orderItemService;

    public OrderItemController(OrderItemService orderItemService) {
        this.orderItemService = orderItemService;
    }

    @GetMapping("/orderItem")
    public ResponseEntity<?> getAll(){
        return orderItemService.getAll();
    }

    @GetMapping("/orderItem/{id}")
    public ResponseEntity<?> getByID(@PathVariable int id){
        return orderItemService.getByID(id);
    }

    @GetMapping("/OrderItemsByCustomerOrderID")
    public ResponseEntity<?> getOrderItemsByCustomerID(@RequestHeader("customerID") Integer customerID){
        return ResponseEntity.ok(orderItemService.getByCustomerID(customerID));
    }

    @DeleteMapping("/orderItem/{id}")
    public void deleteByID(@PathVariable int id){
        orderItemService.deleteByID(id);
    }

    @PostMapping("/orderItem")
    public ResponseEntity<?> create(@RequestBody OrderItem orderItem){
        return ResponseEntity.ok(orderItemService.save(orderItem));
    }


}
