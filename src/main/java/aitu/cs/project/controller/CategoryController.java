package aitu.cs.project.controller;

import aitu.cs.project.model.Account;
import aitu.cs.project.model.Category;
import aitu.cs.project.service.CategoryService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class CategoryController {
    private final CategoryService categoryService;

    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping("/category")
    public ResponseEntity<?> getAll(){
        return categoryService.getAll();
    }

    @GetMapping("/category/{id}")
    public ResponseEntity<?> getByID(@PathVariable int id){
        return categoryService.getByID(id);
    }

    @DeleteMapping("/category/{id}")
    public void deleteByID(@PathVariable int id){
        categoryService.deleteByID(id);
    }

    @PostMapping("/category")
    public ResponseEntity<?> create(@RequestBody Category category){
        return ResponseEntity.ok(categoryService.save(category));
    }


}
