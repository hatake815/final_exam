ALTER TABLE Customer ADD CONSTRAINT "Customer_fk0" FOREIGN KEY ("account_id") REFERENCES Account("account_id");


ALTER TABLE CustomerOrder ADD CONSTRAINT "CustomerOrder_fk0" FOREIGN KEY ("customer_id") REFERENCES Customer("customer_id");
ALTER TABLE CustomerOrder ADD CONSTRAINT "CustomerOrder_fk1" FOREIGN KEY ("delivery_id") REFERENCES Delivery("delivery_id");

ALTER TABLE OrderItem ADD CONSTRAINT "OrderItem_fk0" FOREIGN KEY ("order_id") REFERENCES CustomerOrder("id");
ALTER TABLE OrderItem ADD CONSTRAINT "OrderItem_fk1" FOREIGN KEY ("product_id") REFERENCES Product("product_id");

ALTER TABLE Product ADD CONSTRAINT "Product_fk0" FOREIGN KEY ("model_id") REFERENCES Model("model_id");


ALTER TABLE Model ADD CONSTRAINT "Model_fk0" FOREIGN KEY ("category_id") REFERENCES Category("category_id");

ALTER TABLE Category ADD CONSTRAINT "Category_fk0" FOREIGN KEY ("storage_id") REFERENCES Storage("storage_id");

ALTER TABLE Employee ADD CONSTRAINT "Employee_fk0" FOREIGN KEY ("account_id") REFERENCES Account("account_id");
ALTER TABLE Employee ADD CONSTRAINT "Employee_fk1" FOREIGN KEY ("factory_id") REFERENCES Factory("factory_id");
ALTER TABLE Employee ADD CONSTRAINT "Employee_fk2" FOREIGN KEY ("speciality_id") REFERENCES Category("category_id");