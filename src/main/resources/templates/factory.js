var app = angular.module('factory-page', []);

app.controller('FactoryCtrl', function ($scope, $http){
    /* took OrderItems and CustomerOrder */
    $scope.customerOrder = [];
    $scope.getCustomerOrder = function(){
        $http({
            url: 'http://127.0.0.1:8080/customerOrder',
            method: 'GET',
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json",
            }
        }).then(function (response){
            $scope.customerOrder = response.data;
            console.log(response);

        }, function (response){
            console.log(response);
        })
    }

    $scope.getCustomerOrder();

    $scope.getOrdersByStatus = function (status) {
        $http({
            url: 'http://127.0.0.1:8080/customerOrder/status/' + status,
            method: 'GET',
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json"
            }
        }).then(function (response){
            console.log(response);
            $scope.customerOrder = response.data;
        }, function (response){
            console.log(response);
        })

    };

    /* -- change status -- */
    $scope.sent = function (order) {
        $http({
            url: 'http://127.0.0.1:8080/sentOrder',
            method: 'POST',
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json",
                "token": order.token
            }
        }).then(function (response){
            console.log(response);
            $scope.getCustomerOrder();
        }, function (response){
            console.log(response);
        })
    };

    n = new Date();
    y = n. getFullYear();
    m = n. getMonth() + 1;
    d = n. getDate();
    t = document. getElementById("date"). innerHTML = y + "-" + m + "-" + d;

    const date1 = new Date(t);
    const date2 = new Date('2020-11-02');
    const diffTime = Math.abs(date2 - date1);
    const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
    if(diffDays>7){
        cancel = document. getElementById("cancel1");
    }

    $scope.delete = function(tokken){
        $http({
            url: 'http://127.0.0.1:8080/delete',
            method: 'DELETE',
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json",
                "token": tokken
            }
        }).then(function (response){
            console.log(response);
        }, function (response){
            console.log(response);
        })
    }

});